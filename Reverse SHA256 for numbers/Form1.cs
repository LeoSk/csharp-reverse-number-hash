﻿using System;
using System.Security.Cryptography;
using System.Windows.Forms;
using System.Text;

namespace Reverse_SHA256_for_numbers
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string Sha256(string text)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(text);
            SHA256Managed hashstring = new SHA256Managed();
            byte[] hash = hashstring.ComputeHash(bytes);
            string hashString = string.Empty;
            foreach (byte x in hash)
            {
                hashString += String.Format("{0:x2}", x);
            }
            return hashString;
        }

        private void run_Click(object sender, EventArgs e)
        {
            for (int i = 0; i<9999999; i++)
            {
                if (Sha256(i.ToString()) == input.Text)
                {
                    output.Text = i.ToString();
                    break;
                }
            }
        }
    }
}
